package A2013.q1;

public class Rectangle extends Shape {
	
	private int width;
	private int height;
	
	public Rectangle(){
		width=0;
		height=0;
		
	}
	
	public Rectangle(int w, int h){
		width=w;
		height=h;
		
	}

}
