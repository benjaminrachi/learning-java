package A2013.q3;
import java.util.*;

public class TwoWheelVehicle {

	protected String model;	
	protected String color;	
	protected  double maxSpeed;
	protected Date manDate;  
	
	
	public TwoWheelVehicle(String m, String c, double speed, Date d){
		this.model=m;
		this.color=c;
		this.maxSpeed=speed;
		this.manDate=d;
	}
	
	public void setMaxSpeed(double sp){
		this.maxSpeed=sp;
	}
	
	public double getMaxSpeed(){
		return maxSpeed;
	}
	
	public String toString(){
		String s= this.model+" "+this.color+" "+String.valueOf(this.maxSpeed)+"\n";
		//this.maxSpeed.toString();
		return s;
	}

}
