package A2013.q3;
//import java.util.*;

public class Store {
	
	public TwoWheelVehicle [] arr;
	private int counter=0;
	private double maxSp=0;
	
	public Store(int n){
		arr= new TwoWheelVehicle[n];
	}
	
	public int countPairsOfMaxSpeed(){
		for(int i=0; i<arr.length; i++){
			if(arr[i].getClass() != Bicycle.class) continue;
			
			else{
				if(arr[i].getMaxSpeed() > maxSp){
					maxSp=arr[i].getMaxSpeed();
					counter=1;
				}
				
				else if(arr[i].getMaxSpeed() == maxSp){
					counter++;
				}
			}
		}
		return counter;
	}
	
	public void show(){
		for(int i=0; i<arr.length; i++ ){
			arr[i].toString();
			System.out.println();
		}
	}
	
	

}
