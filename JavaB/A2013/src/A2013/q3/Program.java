package A2013.q3;
import java.util.*;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Date d1 = new GregorianCalendar(2013, Calendar.MARCH, 25).getTime();
		Bicycle b1= new Bicycle("BMX", "RED", 90, d1, false);
		
		Date d2 = new GregorianCalendar(2013, Calendar.APRIL, 27).getTime();
		Bicycle b2= new Bicycle("BMX", "BLUE", 90, d2, false);
		
		b2.setMaxSpeed(100);
		
		Date d3 = new GregorianCalendar(2013, Calendar.MARCH, 27).getTime();
		Bicycle b3= new Bicycle("BMX", "RED", 120, d3, false);
		
		Date d4 = new GregorianCalendar(2013, Calendar.FEBRUARY, 25).getTime();
		Bicycle b4= new Bicycle("BMX", "RED", 120, d4, false);
		
		Date d5 = new GregorianCalendar(2013, Calendar.MARCH, 25).getTime();
		MotorCycle m1= new MotorCycle("Harley", "BLACK", 300, d5, 20215542);
		
		MotorCycle m2= new MotorCycle("Harley", "BUE", 200, d5, 955526952);
		
		Store store= new Store(6);
		store.arr[0]=b1;
		store.arr[1]=m1;
		store.arr[2]=b2;
		store.arr[3]=b3;
		store.arr[4]=b4;
		store.arr[5]=m2;
		
		store.show();
		
		System.out.println(store.countPairsOfMaxSpeed());
		

	}

}
