package A2013.q4;

import java.util.*;
import java.util.Map.Entry;

public class Program {
	public static boolean isPath(HashMap<String, List<String>> m, List<String> p) {
		Iterator<String> it = p.iterator();
		String s = it.next();
		while (it.hasNext()) {
			List<String> v = m.get(s);
			String x = it.next();
			if (!v.contains(x))
				return false;
			else
				s = x;
		}
		return true;
	}
/*
	public static void removeDuplicates(HashMap<String, List<String>> m) {
		Set<String> keys = m.keySet();
		Vector<String> empty = new Vector<String>();
		for (String s : keys) {
			List<String> vs = m.get(s);
			for (String v : vs) {
				List<String> vv = m.get(v);
				vv.remove(s);
				if (vv.isEmpty())
					empty.add(v);
			}
		}
		for (String x : empty)
			m.remove(x);
	}
*/
	public static void main(String[] args) {
		HashMap<String, List<String>> m = new HashMap<String, List<String>>();

		//building the map like in the question
		List<String> a = new Vector<String>();
		a.add("B");
		a.add("D");
		a.add("E");

		m.put("A", a);

		a = new Vector<String>();
		a.add("A");
		a.add("C");
		
		m.put("B", a);

		a = new Vector<String>();
		a.add("B");
		a.add("D");
		
		m.put("C", a);

		a = new Vector<String>();
		a.add("A");
		a.add("C");
		a.add("E");
		
		m.put("D", a);

		a = new Vector<String>();
		a.add("A");
		a.add("D");
		
		m.put("E", a);
		
		//calling the function with the vector "A D C B"
		a = new Vector<String>();
		a.add("A");
		a.add("D");
		a.add("C");
		a.add("B");
		System.out.println(isPath(m, a));

		//calling the function with the vector "A D B C"
		a = new Vector<String>();
		a.add("A");
		a.add("D");
		a.add("B");
		a.add("C");
		System.out.println(isPath(m, a));
/////////////////////////////////////////////////////////////////////////////
	/*
		removeDuplicates(m);

		for (String s : m.keySet()) {
			System.out.print(s + ":");
			for (String x : m.get(s))
				System.out.print(x + " ");
			System.out.println();
		}
		
		for (Entry<String,List<String>> s : m.entrySet()) {
			System.out.print(s.getKey()+":");
			for (String x : s.getValue())
				System.out.print(x+" ");
					System.out.println();
		}
			
	*/
	}

}

