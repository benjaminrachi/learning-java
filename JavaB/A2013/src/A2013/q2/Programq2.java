package A2013.q2;

import java.util.*;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.Calendar;

public class Programq2 {

	public static void main(String[] args) throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		
		TaskList list1= new TaskList(3);
		
		//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		//Date d1 = sdf.parse("10/07/2017");
		//Date d1 = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
		//Task task1= new Task("study for the exam", d1);
		
		Date d1 = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
		Task task1= new Task("study for the exam", d1);
		
		Date d2 = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
		Task task2= new Task("pass the exam", d2);
		
		Date d3 = new GregorianCalendar(2017, Calendar.AUGUST, 10).getTime();
		Task task3= new Task("study for the exam", d3);
		
		list1.add(task1);
		list1.add(task2);
		list1.add(task3);
		
		list1.toString();
		
		TaskList list2= list1.clone();
		list2.toString();
		
		list1.iterator().toString();
		
	}

}

