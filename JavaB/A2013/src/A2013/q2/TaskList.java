package A2013.q2;

import java.util.*;

public  class  TaskList implements Iterable{
	  private Task[]   tasks; // list of tasks
	  private    int   counter; // next  task  index
	  
	  public  TaskList (int  n) {
	   tasks  = new Task[n];
	  }
	  
	  public void add(Task t){
		  tasks[counter]=t;
		  counter++;
	  }
	  
	  public String toString(){
		  String s="[";
		  
		  for(int i=0; i< this.counter; i++){
			  s= s+tasks[i].toString();
		  }
		  
		  s= s+"]";
		  return s;
	  }
	  
	  protected TaskList clone() throws CloneNotSupportedException {
			TaskList t = (TaskList) super.clone();
			t.tasks = (Task[]) t.tasks.clone();
			
			for (int i = 0; i < counter; i++){
				t.tasks[i] = (Task) t.tasks[i].clone();
			}
			
			return t;
	  }

	  
	  public boolean equals(Object o){
		  int i,j;
		  if (this==o) return true;
		  if (o==null) return false;
		  if (o.getClass() != this.getClass()) return false;
		  
		  TaskList t= (TaskList) o;
		  
		  for( i=0; i<this.counter; i++){
			  if(this.tasks[i].isOpen()){
				  for(j=0; j<t.counter; j++){
					  if(this.tasks[i].equals(t.tasks[j])) break;
				  }
				  if (j == t.counter) return false;
			  }
		  }
		  return true;
	  }
	  
	  public int getCounter(){
		  return this.counter;
	  }  
	  
	  public Task getTask(int idx){
		  return tasks[idx];
	  }
		

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return TaskListIt(this);
	}

	
	}
	  
	  
}
