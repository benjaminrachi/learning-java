package A2013.q2;

import java.util.Iterator;

public interface TaskListIterator extends Iterator<Task> {
	
	void setScanType();

}
