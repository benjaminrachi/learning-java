package A2013.q2;
import java.util.Scanner;

import test.TaskListIterator;

public class TaskListIt implements TaskListIterator{

	private int done= this.findNextDone(); //idx of the next done task in the array list
	private int nondone= this.findNextNonDone(); //idx of the next none done task in the array list
	private int all=0; //idx of the next task in the array list
	private int type=0;
	private TaskList list;
	
	public TaskListIt(TaskList t){
	this.list=t;	
	setScanType();
	}
	
	@Override
	public boolean hasNext() { // returns true if the list has another elements corresponding to  the scan
		// TODO Auto-generated method stub
		
		if(type==3){ //scan all the elements
			return all<this.list.getCounter();
		}
		else if(type==2){ //scan the done elements
			return (done != -1);
		}
		
		else //scan the non-done elements
			return (nondone != -1);
		
		//return false;
	}

	@Override
	public Task next() { // returns the next element in the list corresponding to the scan
		// TODO Auto-generated method stub
		
		int temp;
		
		if (! hasNext()) return null;
		
		else{
			if(type==3){ //scan all the elements
				return this.list.getTask(all++);
			}
			
			else if(type==2){ //scan the done elements
				temp=done;
				done= findNextDone(done);
				return this.list.getTask(temp);
			}
			
			else //scan the non-done elements
				temp=nondone;
				nondone= findNextNoneDone(nondone);
				return this.list.getTask(temp);
		}
		
	}
	
	public int findNextDone(int currentidx){ //returns the index of the next element corresponding to the scan or -1 if there is not.
		
		for(int i=currentidx+1; i< this.list.getCounter(); i++){
			if( ! list.getTask(i).isOpen()) return i;
			
		}
		return -1;
	}
	
	public int findNextNonDone(int currentidx){
		
		for(int i=currentidx+1; i< this.list.getCounter(); i++){
			if( list.getTask(i).isOpen()) return i;
			
		}
		return -1;
	}

	@Override
	public void setScanType() {
		// TODO Auto-generated method stub
		System.out.println("Enter the type of scan you want:\n 1_all\n 2_done\n 3_non done");
		
		Scanner scanIn = new Scanner(System.in);
	       this.type = scanIn.nextInt();

	   scanIn.close();            
       System.out.println("You choose scan of type "+ this.type);
		
		//this.type=type;
	}
	
	public void show(){
		this.list.toString();
	}

	
}
