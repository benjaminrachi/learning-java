package A2013.q2;

import java.util.Date;

public  class Task {
   private  String  desc;   
   private   Date  when;
   private    Boolean   done; 
   
   public  Task (String  desc, Date when) {
    this.desc = desc;
    this.when = when;
   }
   
   public String toString(){
	   String s="("+desc+", "+when+")";
	   return s;
   }
   
   public Task clone() throws CloneNotSupportedException{
	   
	   Task t= (Task)super.clone();
	   t.when= (Date)t.when.clone();
	   return t;
   }
   
   public boolean isOpen(){ //returns true if the task is still opened= not done yet
	   return !done;
   }
   
   public boolean equals (Object o){
	   
	   if(this==o) return true;
	   
	   if(this.getClass() != o.getClass()) return false;
	   
	   Task t= (Task) o;
	   if(this.desc.equals(t.desc)&& this.when.equals(t.when)&& this.done.equals(t.done)) return true;
	   
	   return false;
   }
   
   
}
