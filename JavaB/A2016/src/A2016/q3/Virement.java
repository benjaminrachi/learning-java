package A2016.q3;

public class Virement extends Transaction {
	
	private int destinationAccountNo;
	
	public Virement(Bank b, int acc, int s, int d){
		super(b, acc, s);
		this.destinationAccountNo=d;
		
	}

	@Override
	public boolean execute() {
		// TODO Auto-generated method stub
		boolean flag;
		flag= myBank.getAccountByNumber(sourceAccountNo).withdraw(sum);
		
		if (! flag) return false;
		
		myBank.getAccountByNumber(destinationAccountNo).deposit(sum);
		return true;
		
	}

}
