package A2016.q3;

public abstract class Transaction {

	protected Bank myBank;
	protected int sourceAccountNo;
	protected int sum;
	
	public Transaction(Bank bank, int accNo, int sum){
		this.myBank=bank;
		this.sourceAccountNo=accNo;
		this.sum= sum;
	}
	
	public String toString(){
		return "Account (number=" + sourceAccountNo + ", sum=" + sum + ")";
	}
	
	public abstract boolean execute();
}
