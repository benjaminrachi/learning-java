package A2016.q3;

public class Account {

	private int accNo;
	private int balance;
	
	public Account(int accNo, int balance){
		this.accNo=accNo;
		this.balance=balance;
	}
	
	public int getAccNo(){
		return accNo;
	}
	
	public int getAccount(){
		return this.accNo;
	}
	
	public int getBalance(){
		return this.balance;
	}
	
	public void deposit (int x){
		balance+=x;
	}
	
	public boolean withdraw(int x){
		if(balance -x <0) return false;
		
		balance-=x;
		return true;
	}
	
	@Override
	public String toString() {
		return "Account (number=" + accNo + ", balance=" + balance + ")";
	}
}
