package A2016.q3;

public class Bank {

	private String name;
	private Account[] accs; //size=20 by default
	private int countAcc=0;
	private Transaction[]trans; //size=20 by default
	private int countTrans=0;
	
	public Bank(){
		this.name="Bank";
		accs= new Account[20];
		trans= new Transaction[20];
	}
	
	public Bank(String name){
		this.name=name;
		accs= new Account[20];
		trans= new Transaction[20];
	}
	
	public Bank(String name, int sizeAcc, int sizeTrans){
		this.name=name;
		accs= new Account[sizeAcc];
		trans= new Transaction[sizeTrans];
	}
	
	public void addAc(Account acc){
		if( countAcc >= accs.length) return;
		
		accs[countAcc++]=acc;
	}
	
	public void addTr(Transaction tr){
		if( countTrans >= trans.length) return;
		
		trans[countTrans++]=tr;
	}
	
	public Account getAccountByNumber(int accNo){
		for (int i=0; i<countAcc; i++){
			if (accs[i].getAccNo()==accNo) return accs[i];
		}
		
		return null;
	}
	
	public String toString(){
		return this.name;
	}
	
	public String toStringAcc(){
		String res="Bank "+this.name+"[";
		
		for(int i=0; i<this.countAcc; i++){
			res=res+this.accs[i].toString();
		}
		
		res=res+"]";		
		return res;
	}
	
	public String toStringTr(){
		String res="Bank "+this.name+"[";
		
		for(int i=0; i<this.countTrans; i++){
			res=res+this.trans[i].toString();
		}
		
		res=res+"]";		
		return res;
	}
	
	public void showAcc(){
		System.out.println(this.toStringAcc());
	}
	
	public void showTr(){
		System.out.println(this.toStringTr());
	}
	
	public Transaction[] execute(){
		int countBad=0;
		
		Transaction[] bad=new Transaction[countTrans];
		
		for(int i=0; i< this.countTrans; i++){
			if (! trans[i].execute()){
				bad[countBad++]=trans[i];
			}
		}
		
		for(int i=countBad; i<countTrans; i++){
			bad[i]=null;
		}
		
		return bad;
	}
}
