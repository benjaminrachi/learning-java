package A2016.q3;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Bank b1 = new Bank("Leumi", 5, 10);
		
		Account acc1= new Account(25845, 5000);
		Account acc2= new Account(78542, 15000);
		Account acc3= new Account(78458, 85000);

		b1.addAc(acc1);
		b1.addAc(acc2);
		b1.addAc(acc3);
		
		b1.showAcc();

		Transaction t1 = new Deposit(b1, 1234, 2250);
		Transaction t2 = new Deposit(b1, 5678, 900);
		Transaction t3 = new Deposit(b1, 3081, 1500);

		Transaction t4 = new Withdraw(b1, 1234, 300);
		Transaction t5 = new Withdraw(b1, 5678, 150);
		Transaction t6 = new Withdraw(b1, 3081, 450);

		Transaction t7 = new Virement(b1, 5678, 3081, 150);
		
		b1.addTr(t1);
		b1.addTr(t2);
		b1.addTr(t3);
		b1.addTr(t4);
		b1.addTr(t5);
		b1.addTr(t6);
		b1.addTr(t7);
		
		b1.showTr();
		
		Transaction[] badTransactions=b1.execute();
		

	}

}
