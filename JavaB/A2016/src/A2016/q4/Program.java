package A2016.q4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
//import java.util.*;
import java.util.Vector;

public class Program {
	
	public static <T extends Comparable<T>> boolean rezef(HashMap<Integer, List<T>> m) {

		Set<Entry<Integer, List<T>>> es = m.entrySet();

		int sum = 0;
		for (Entry<Integer, List<T>> entry : es) {
			sum = check(entry.getValue());
			if (entry.getKey() > sum)
				return false;
		}
		return true;
	}

	public static <T extends Comparable<T>> int check(List<T> ls) {

		int m = 1;
		int k = 1;
		for (T t : ls) {
			if (k < ls.size()) {
				T t2 = ls.get(k++);
				if (t.compareTo(t2) == 0) {
					m++;
				}
			}
		}

		if (m == 1)
			return 0;

		return m;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		HashMap<Integer, Vector<Integer>> map1= new HashMap<>();
		
		Vector<Integer> v1= new Vector<Integer>();
		v1.add(4);
		v1.add(5);
		v1.add(5);
		v1.add(6);
		map1.put(2, v1);
		
		Vector<Integer> v1= new Vector<Integer>();
		v1.add(2);
		v1.add(1);
		v1.add(1);
		v1.add(1);
		v1.add(1);
		map1.put(3, v1);
		
		Vector<Integer> v1= new Vector<Integer>();
		v1.add(7);
		v1.add(7);
		v1.add(8);
		map1.put(2, v1);
		
		System.out.println(rezef(map1));
		
		HashMap<Integer, Vector<Integer>> map2= new HashMap<>();
		
		Vector<Integer> v1= new Vector<Integer>();
		v1.add(4);
		v1.add(5);
		v1.add(5);
		v1.add(6);
		map2.put(2, v1);
		
		Vector<Integer> v1= new Vector<Integer>();
		v1.add(2);
		v1.add(1);
		v1.add(1);
		v1.add(1);
		v1.add(1);
		map2.put(3, v1);
		
		Vector<Integer> v1= new Vector<Integer>();
		v1.add(7);
		v1.add(6);
		v1.add(8);
		v1.add(7);
		map2.put(2, v1);
		
		System.out.println(rezef(map2));

	}

}
