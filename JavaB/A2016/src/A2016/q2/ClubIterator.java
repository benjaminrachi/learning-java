package A2016.q2;

import java.util.Iterator;
import java.util.Scanner;


public class ClubIterator implements Iterator<Player> {
	private FC club;
	private int minLevel;
	private int index=-1;
	
	public ClubIterator(FC club) {
		// TODO Auto-generated constructor stub
		this.club=club;
		setMinLevel();
	}
	
	public void setMinLevel(){
		System.out.println("Enter the minimum level you want to scan");
		Scanner sc = new Scanner(System.in);
		minLevel = sc.nextInt();
		sc.close();
	}
	
	public int findNextIdx(){
		for (int i=this.index+1; i<club.getCounter(); i++){
			if( club.getElement(i).getLevel()> this.minLevel) return i;
		}
		
		return -2;
	}
	
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		if (this.index==-2) return false;
		
		//int temp= this.findNextIdx(this.index);
		int temp= this.findNextIdx();
		
		if (temp==-2) return false;
		
		return true;
	}
	
	@Override
	public Player next() {
		// TODO Auto-generated method stub
		//this.index=this.findNextIdx(this.index);
		this.index=this.findNextIdx();
		
		if (index==-2) return null;
		return club.getElement(index);
	}
	
	public void remove(){
		
	}

}
