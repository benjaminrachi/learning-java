package A2016.q2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class FC implements Cloneable, Iterable<Player>{

	private String clubName;
	private int counter;
	private Player[] players;
	
	public FC(String name){
		this.clubName=name;
		players= new Player[100];
		this.counter=0;
	}
	
	public int getCounter(){
		return this.counter;
	}
	
	public Player getElement(int k){
		return this.players[k];
	}
	
	public int pickBadPlayer(){
		int minLevel=20;
		int maxAge=0;
		int idx=0;
		
		for(int i=0; i< counter; i++){
			if (players[i].getLevel() < minLevel){
				minLevel=players[i].getLevel();
				maxAge=players[i].getAge();
				idx=i;
			}
			
			else if (players[i].getLevel() == minLevel){
				if (players[i].getAge() > maxAge){
					maxAge=players[i].getAge();
					idx=i;
				}
			}
		}
		return idx;
	}
	
	public String toString(){
		String res="";
		for(int i=0; i< this.counter; i++){
			res= res+players[i].toString();
		}
		return res;
	}

	public void show(){
		System.out.println(this.toString());
	}
	
	public void addPlayer(Player p){
		if (counter < 100){
			players[counter++]=p;
		}
		
		else
			players[this.pickBadPlayer()]=p;
	}
	
	public boolean equals (Object o){
		
		if (this==o) return true;
		
		if(o.getClass() != FC.class) return false;
		
		FC club =(FC) o;
		if ( (! club.clubName.equals(this.clubName)) || (club.counter != this.counter) ) return false;

		
		ArrayList<Player> Listplayers= new ArrayList<Player>(Arrays.asList(club.players));
		
		for(int i=0; i< counter; i++){
			if (! Listplayers.contains(this.players[i])) return false;
		}
		
		return true;

	}
	
	public boolean equals2 (Object o){
		boolean flag;
		
		if (this==o) return true;
		
		if(o.getClass() != FC.class) return false;
		
		FC club =(FC) o;
		if ( (! club.clubName.equals(this.clubName)) || (club.counter != this.counter) ) return false;

		
		for(int i=0; i< this.counter; i++){
			flag=false;
			for(int j=0; j<club.counter; j++){
				if (this.players[i].equals(club.players[j])) {
					flag=true;
					break;
				}
								
			}
			if (!flag) return false;
			
		}
		return true;

	}

	public FC clone() throws CloneNotSupportedException{
		FC c= (FC)super.clone();
		c.players=this.players.clone();
		
		for(int i=0; i<this.players.length; i++){
			c.players[i]=this.players[i].clone();
		}
		return c;
	}

	
	@Override
	public Iterator<Player> iterator() {
		// TODO Auto-generated method stub
		return new ClubIterator(this);
	}

	
}
