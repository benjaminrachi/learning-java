package A2016.q2;

public class Program {

	public static void main(String[] args) throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		
		Player p1= new Player("Maradonna", 17, 65);
		Player p2= new Player("Ronaldo", 18, 45);
		Player p3= new Player("Zidane", 18, 47);
		Player p4= new Player("Messi", 19, 40);
		Player p5= new Player("Kaka", 15, 25);
		Player p6= new Player("Henry", 20, 40);
		Player p7= new Player("Trezeguet", 10, 35);

		FC club1= new FC("PSG");
		club1.addPlayer(p1);
		club1.addPlayer(p2);
		club1.addPlayer(p3);
		club1.addPlayer(p4);
		club1.addPlayer(p5);
		club1.addPlayer(p6);
		club1.addPlayer(p7);
		
		club1.show();
		
		FC club2= club1.clone();
		club2.show();
		
		ClubIterator ci= (ClubIterator) club2.iterator();
		
		while(ci.hasNext()){
			ci.next().show();
		}
	}

}
