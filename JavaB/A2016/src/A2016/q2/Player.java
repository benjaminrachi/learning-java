package A2016.q2;

public class Player implements Cloneable{

	private String name;
	private int level;
	private int age;
	
	public Player(String name, int level, int age){
		this.name=name;
		this.level=level;
		this.age=age;
	}
	
	public int getLevel(){
		return this.level;
	}
	
	public int getAge(){
		return this.age;
	}

	public String getName(){
		return this.name+"/n";
	}
	
	public boolean equals(Object o){
		
		if (this==o) return true;
		
		if(o.getClass() != Player.class) return false;
		
		Player p= (Player) o;
		
		return ((this.name.equals(p.getName())) && (this.age==p.getAge()) && (this.level==p.getLevel()));
		
	}

	public Player clone() throws CloneNotSupportedException{
		Player c= (Player) super.clone();
		return c;
	}

	public String toString(){
		return name;
	}


	public void show() {
		// TODO Auto-generated method stub
		System.out.println(this.toString());
	}
}
