package A2012.q2;

import java.util.Iterator;

public class BigDecimal implements Cloneable, Comparator, Iterable<Integer>{

	private  int[]   digits;
	private  int     pos;
	private  boolean    negative;
	
	public   BigDecimal (String  s) {} 
	
	public String toString(){
		String res="";
		
		if (negative){
			res= res+"-";
		}
		
		for(int i=0; i<digits.length; i++){
			if (i== pos) res= res+".";
			res=res+ String.valueOf(digits[i]);
		}
		return res;
	}
	
	public BigDecimal clone() throws CloneNotSupportedException{
		BigDecimal c= (BigDecimal) super.clone();
		c.digits= this.digits.clone();
		
		for(int i=0; i<digits.length; i++){
			c.digits[i]=this.digits[i];
		}
		
		return c;
	}
	
	public   boolean  equals (Object o){
		if(o==this) return true;
		
		if(o.getClass() != BigDecimal.class) return false;
		
		BigDecimal b= (BigDecimal) o;
		
		for(int i=0; i<digits.length; i++){
			if (i== pos+2) break;
			
			if (b.digits[i]!=this.digits[i]) return false;
		}
		return true;
	}

	public int getPosition(){
		return pos;
	}
	
	public int getElementK(int k){
		return this.digits[k];
	}
	
	@Override
	public Iterator<Integer> iterator() {
		// TODO Auto-generated method stub
		return new BigDecimalIt(this);
	}
	

}
