package A2012.q2;

import java.util.Iterator;

public class BigDecimalIt implements Iterator<Integer>{
	private BigDecimal bd;
	private int position;
	private int idx;
	
	public BigDecimalIt(BigDecimal num) {
		// TODO Auto-generated constructor stub
		this.bd=num;
		this.position=num.getPosition();
		this.idx=this.position-1;
	}
	
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return idx>=0;
	}

	@Override
	public Integer next() {
		// TODO Auto-generated method stub
		return bd.getElementK(idx--);
	}

}
