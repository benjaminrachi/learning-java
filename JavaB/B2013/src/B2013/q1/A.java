package B2013.q1;

public class A {
	protected int num;

	public A(int n) {
		this.num = n;
	}

	protected void f() {
	  System.out.println(this.num);
	}

	public void run() {
			f();
	}
}
