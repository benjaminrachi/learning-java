package B2013.q1;

public class List<T> {

	private ListNode<T> firstNode;
	private ListNode<T> lastNode;
	private String name;

	public List() {
		this("list");
	}

	public List(String listName) {
		name = listName;
		firstNode = lastNode = null;
	}

	public void insertAtFront(T item) {
		if (isEmpty())
			firstNode = lastNode = new ListNode<T>(item);
		else
			firstNode = new ListNode<T>(item, firstNode);
	}

	public void insertAtBack(T item) {
		if (isEmpty())
			firstNode = lastNode = new ListNode<T>(item);
		else
			lastNode = lastNode.nextNode = new ListNode<T>(item);
	}

	public boolean isEmpty() {
		return firstNode == null;
	}

	public List<T> RecUnknown() {
		List<T> r = new List<T>();
		if (this.isEmpty())
			return r;
		r.firstNode = this.firstNode.RecUnknown();
		return r;
	}
	
	public List<T> unknown(){
		List<T> r =new List<T>();
		ListNode<T> temp= firstNode;
		
		while(temp != null){
			if ( temp.nextNode.data.equals(temp.data)){
				temp= temp.nextNode;
				continue;
			}
			
			//ListNode<T> node = new ListNode<T>(temp.data);
			r.insertAtBack(temp.data);
			temp= temp.nextNode;
		}
		return r;
			
	}
}
