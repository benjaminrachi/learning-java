package B2013.q1;

public class ListNode<T> {

	 public T data;
	 public ListNode<T> nextNode;
		
		
	public ListNode(T o){
		this(o,null);
	 }
	public ListNode(T o,ListNode<T> node){
	     data=o;
	     nextNode=node;
	}
		
	public ListNode<T> RecUnknown() {
	 if (nextNode==null)
	  return new ListNode<T>(data);
	 else {
	  ListNode<T> s;
	  s = nextNode.RecUnknown();
	  if (data.equals(nextNode.data))
	     return s;
	  else {		    
	     ListNode<T> t;
	     t = new ListNode<T>(data);
	     t.nextNode = s;
	     return t;
	  }		    		
	 }			  
	}
	
	
}

