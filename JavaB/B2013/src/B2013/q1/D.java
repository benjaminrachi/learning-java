package B2013.q1;

public class D extends C {
	private int num3;

public D(int n, int n3) {
		super(n,n);
		this.num3 = n3;
	}

protected void f() {
		g();
		h();
	}

public void g() {
    super.f();
    System.out.println(this.num3);
    System.out.println(this.num*this.num3);
 }

public void h() {
    super.f();
    System.out.println(this.num3);
    System.out.println(this.num / this.num3);
  }
}

