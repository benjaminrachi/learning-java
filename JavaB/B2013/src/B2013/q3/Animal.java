package B2013.q3;

public class Animal {

	protected String name;	  
	protected String color;	 
	protected  double weight; 
	
	public Animal(String s, String c, double w){
		this.name=s;
		this.color=c;
		this.weight=w;
	}
	
	public void setWeight(double weight){
		this.weight=weight;
	}
	
	public double getWeight(){
		return weight;
	}

}
