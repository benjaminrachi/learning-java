package B2013.q3;

public class Reptile extends Animal {

	private int  len; 
    private  boolean poisonous; 
    
    public Reptile(String s, String c, double w, int l, boolean p){
    	super(s,c,w);
		this.len=l;
		this.poisonous=p;
	}
    
    public boolean isPoisonous(){
    	return poisonous;
    }

}
