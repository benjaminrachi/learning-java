package B2013.q3;

import java.util.HashSet;

public class Zoo {

	private Animal[] Animals;
	private double minWeigthNonPoisonous;
	private int CountMinWeigthNonPoisonous;
	
	public void countNoPoisonous(){
		
		for(int i=0; i< Animals.length; i++){
			if (  ( Animals[i].getClass()==Reptile.class)  &&  ( (Reptile)Animals[i].isPoisonous()==true)  ){
				if(Animals[i].getWeight() < minWeigthNonPoisonous){
					minWeigthNonPoisonous=Animals[i].getWeight();
					CountMinWeigthNonPoisonous=1;
				}
				
				else if(Animals[i].getWeight() == minWeigthNonPoisonous){
					CountMinWeigthNonPoisonous++;
				}
			}
		}
		
		System.out.println("The minimum weight of a non poisonous reptile is "+minWeigthNonPoisonous+" kg, there are "+CountMinWeigthNonPoisonous+" reptiles with this weight");
	}
	
	public int countTypes(){
		HashSet<Class> types= new HashSet<Class>();
		
		for(int i=0; i< Animals.length; i++){
			if(Animals[i]==null) continue;
			
			types.add(Animals[i].getClass());
		}
		
		return types.size();
	}
		
		
}
