package B2013.q2;

public class Movie implements Cloneable{

	  private    String   title; 
	  private   Actor[]   as; 
	  private   Actor[]    ss;
	  private    int   cas; 
	  private    int   css ;  
	  
	  public  Movie (String title, int  n, int  m) {
	   this.title = title;
	   as  = new Actor[n];
	   ss  =  new  Actor[m];
	  }
	  
	  public String toString(){
		  String result= "["+title+": ";
		  
		  for (int i=0; i< cas-1; i++){
			  result= result + as[i].toString();
			  
			  for(int j=0; j<css; j++){
				  if( as[i]==ss[j]){
					  result= result+"*";
				  }
			  }
			  result= result+", ";
		  }
		  
		  result= result + as[cas-1].toString();
		  
		  for(int j=0; j<css; j++){
			  if( as[cas-1]==ss[j]){
				  result= result+"*";
			  }
		  }
		  
		  result= result+"]";
		  
		  return result;
	  }
	  
	  public boolean isStar(Actor a){
			  for(int j=0; j<css; j++){
				  if( a==ss[j]){
					  return true;
				  }
			  }
			  
			  return false;
	  }
	  
	  public Movie clone() throws CloneNotSupportedException{
		  Movie m= (Movie)super.clone();
		  return m;
	  }
}
