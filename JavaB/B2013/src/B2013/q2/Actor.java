package B2013.q2;
import java.util.*;

public class Actor implements Cloneable{

	   private   String  name; 
	   private   Date  bDate;
	   
	   public  Actor (String  name, Date  bDate) {
	    this.name = name;
	    this.bDate = bDate;
	   }
	   
	   public String toString(){
		   return name;
	   }
	   
	   public Actor clone() throws CloneNotSupportedException{
		   Actor a= (Actor)super.clone();
		   return a;
		   
	   }

}
