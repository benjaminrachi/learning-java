package B2013.q4;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

public class Program {

	public static void removeDuplicates(HashMap<String, List<String>> m) {
		Set<String> keys = m.keySet();
		Vector<String> empty = new Vector<String>();
		for (String s : keys) {
			List<String> vs = m.get(s);
			for (String v : vs) {
				List<String> vv = m.get(v);
				vv.remove(s);
				if (vv.isEmpty())
					empty.add(v);
			}
		}
		for (String x : empty)
			m.remove(x);
	}
	
	
	public static void main(String[] args) {
		HashMap<String, List<String>> m = new HashMap<String, List<String>>();

		//building the map like in the question
		List<String> a = new Vector<String>();
		a.add("B");
		a.add("D");
		a.add("E");

		m.put("A", a);

		a = new Vector<String>();
		a.add("A");
		a.add("C");
		
		m.put("B", a);

		a = new Vector<String>();
		a.add("B");
		a.add("D");
		
		m.put("C", a);

		a = new Vector<String>();
		a.add("A");
		a.add("C");
		a.add("E");
		
		m.put("D", a);

		a = new Vector<String>();
		a.add("A");
		a.add("D");
		
		m.put("E", a);
	
	}		
}
